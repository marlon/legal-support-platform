## Infrastructure Requirements
- A server running Debian or Ubuntu, with Docker Compose installed
- Two subdomains which resolve to the server: db.example.com and accounts.example.com

## Configuration
- Copy `grist.env.example` to `grist.env` and update the appropriate values
- Copy `ldap.env.example` to `ldap.env` and update the appropriate values
- Copy `nginx.env.example` to `nginx.env` and update `LETSENCRYPT_EMAIL`
- Copy `grist/example.dex.yaml` to `grist/dex.yaml` and update the appropriate values
	- `bindPW` should be set to the same value as `LLDAP_LDAP_USER_PASS` in `ldap.env`
	- Update references to `dc=example,dc=com` to the application domain
- Create the directory ldap/data
- Copy `ldap/lldap_config.docker_template.toml` to `ldap/data/lldap_config.toml` and update the appropriate values.
	- Be sure to generate a new `jwt_secret`
- Copy `docker-compose.example.yml` to `docker-compose.yml` and update the references to `example.com`

## Running / Setup
- Run `docker-compose up`
- Open https://accounts.example.com in a browser, log in using `admin` and the password set in `ldap.env`
- Create a group called `grist`
- Add the `admin` user to the `grist` group
- Set the email address for the `admin` user to the value set in `grist.env`
- Open https://db.example.com in a browser, log in using `admin`

## Creating new users
- Invite the user's email address using an admin user at https://db.example.com
- Create the user's account in https://accounts.example.com
	- Set their email address to the same value as was invited in the previous step
	- Add the user to the `grist` group
